import Vue from 'vue'
import Vuex from 'vuex'
import shortid from 'shortid'
import * as types from './mutation-types'
Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    todoList: [],
  },

  mutations: {
    /* localstorage setting */
    [types.INITIAL_STORE](state) {
      if (localStorage.getItem('todoList')) {
        state.todoList = JSON.parse(localStorage.getItem('todoList'))
      }
    },

    [types.ADD_TODO](state, todoItem) {
      state.todoList.push(todoItem)
    },

    [types.EDIT_TODO](state, editedTodoItem) {
      let todoEditableIndex = state.todoList.findIndex(item => item.id === editedTodoItem.id)
      if (todoEditableIndex !== -1) state.todoList.splice(todoEditableIndex, 1, editedTodoItem)
    },

    [types.REMOVE_TODO](state, removingTodoId) {
      let todoEditableIndex = state.todoList.findIndex(item => item.id === removingTodoId)
      if (todoEditableIndex !== -1) state.todoList.splice(todoEditableIndex, 1)
    },

    [types.CHANGE_TODO_STATUS](state, todoItem) {
      todoItem.completed = !todoItem.completed
    },

    [types.COMPLETE_ALL](state) {
      state.todoList.forEach((item, i) => {
        Object.assign(state.todoList[i], { completed: true })
      })
    },
  },

  actions: {
    addTodo: ({ commit }, todoText) => {
      commit(types.ADD_TODO, {
        id: shortid.generate(),
        text: todoText,
        completed: false,
      })
    },

    changeTodoStatus: ({ commit }, todoItem) => {
      commit(types.CHANGE_TODO_STATUS, todoItem)
    },

    editTodo: ({ commit }, editedTodo) => {
      commit(types.EDIT_TODO, editedTodo)
    },

    removeTodo: ({ commit }, id) => {
      commit(types.REMOVE_TODO, id)
    },

    completeAll: ({ commit }) => {
      commit(types.COMPLETE_ALL)
    },
  },

  getters: {
    todoList: state => {
      return filterType =>
        state.todoList.filter(item => {
          if (filterType === 'uncompleted') {
            return !item.completed
          } else if (filterType === 'completed') {
            return item.completed
          } else {
            return state.todoList
          }
        })
    },

    allTodosNum: state => {
      return state.todoList.length
    },

    completedTodosNum: state => {
      return state.todoList.filter(item => item.completed === true).length
    },

    unCompletedTodosNum: state => {
      return state.todoList.filter(item => item.completed === false).length
    },
  },
})
