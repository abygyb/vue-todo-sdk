import Vue from 'vue'
import App from './App.vue'
// eslint-disable-next-line
import { EventBus } from './eventBus'
import { store } from './store'
import { INITIAL_STORE } from './store/mutation-types'
import router from './router'

Vue.config.productionTip = false

store.subscribe((mutation, state) => {
  localStorage.setItem('todoList', JSON.stringify(state.todoList))
})

new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  beforeCreate() {
    this.$store.commit(INITIAL_STORE)
  },
  render: h => h(App),
})
