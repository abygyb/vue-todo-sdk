import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import ListFilter from '@/components/ListFilter'

Vue.use(Router)
export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: 'all',
    },
    {
      path: '/',
      name: 'main',
      component: Main,
      children: [
        {
          path: '/all',
          name: 'all',
          component: ListFilter,
        },
        {
          path: '/completed',
          name: 'completed',
          component: ListFilter,
        },
        {
          path: '/uncompleted',
          name: 'uncompleted',
          component: ListFilter,
        },
      ],
    },
  ],
})
